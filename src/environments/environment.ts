// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCmqj6wFtVRKghb146fJfB9BTTjET1X6-4",
    authDomain: "oshop-16324.firebaseapp.com",
    databaseURL: "https://oshop-16324.firebaseio.com",
    projectId: "oshop-16324",
    storageBucket: "oshop-16324.appspot.com",
    messagingSenderId: "529940998252",
    appId: "1:529940998252:web:3c659caef40fb958155049",
    measurementId: "G-KRZ5QRSYPC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
