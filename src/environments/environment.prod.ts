export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCmqj6wFtVRKghb146fJfB9BTTjET1X6-4",
    authDomain: "oshop-16324.firebaseapp.com",
    databaseURL: "https://oshop-16324.firebaseio.com",
    projectId: "oshop-16324",
    storageBucket: "oshop-16324.appspot.com",
    messagingSenderId: "529940998252",
    appId: "1:529940998252:web:3c659caef40fb958155049",
    measurementId: "G-KRZ5QRSYPC"
  }
};
