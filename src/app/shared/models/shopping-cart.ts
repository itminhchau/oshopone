import { ShoppingCartItem } from './shopping-cart-item';
import { Product } from './product';

export class ShoppingCart {
    items: ShoppingCartItem[] = [];


    constructor(public itemsMap: ShoppingCartItem[]) {
        // this.itemsMap = itemsMap || {};
        // console.log(itemsMap)
        for (let product of itemsMap) {
            this.items.push(new ShoppingCartItem(product));
        }
    }

    getQuantity(product: Product) {
        for (let productin of this.itemsMap) {
            let item = productin;
            if (item.key === product.key)
                return item ? item.quantity : 0;
        }
        return 0;
    }


    get totalPrice() {
        let sum = 0;
        for (let productId in this.items)
            sum += this.items[productId].totalPrice;
        return sum;
    }

    get totalItemsCount() {
        let count = 0;
        // console.log();

        for (let productId in this.items)
            // console.log(productId)
            count += this.items[productId].quantity;
        return count;
    }
}