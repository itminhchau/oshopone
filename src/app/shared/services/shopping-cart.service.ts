import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Product } from 'shared/models/product';
import { take, map } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { ShoppingCart } from 'shared/models/shopping-cart';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor(private db: AngularFireDatabase) { }
  private create() {
    return this.db.list('/shopping-carts').push({
      dateCreated: new Date().getTime()
    });
  }

  private getItem(cartId: string, productId: string) {
    return this.db.object('/shopping-carts/' + cartId + '/items/' + productId);
  }

  async getCart(): Promise<Observable<ShoppingCart>> {
    let cartId = await this.getOrCreateCartId();
    return this.db.list<ShoppingCart>('/shopping-carts/' + cartId + '/items').valueChanges()
      .pipe(map((x: any) => {
        // console.log(x);
        return new ShoppingCart(x)
      }));
  }

  async clearCart() {
    let cartId = await this.getOrCreateCartId();
    this.db.object('/shopping-carts/' + cartId + '/items').remove();
  }

  private async getOrCreateCartId(): Promise<string> {
    let cartId = localStorage.getItem('cartId');
    if (cartId) return cartId;

    let result = await this.create();
    localStorage.setItem('cartId', result.key);
    return result.key;
  }
  async addToCart(product: Product) {
    this.updateItemQuantity(product, 1);
  }
  async removeFromCart(product: Product) {
    this.updateItemQuantity(product, -1);
  }
  private async updateItemQuantity(product: Product, change: number) {
    let cartId = await this.getOrCreateCartId();
    let item$ = this.getItem(cartId, product.key);
    item$.valueChanges().pipe(take(1)).subscribe((item: any) => {

      if (item) {
        let quantity = (item.quantity || 0) + change;
        if (quantity === 0) item$.remove();
        else
          item$.update({
            key: product.key,
            title: product.title,
            imageUrl: product.imageUrl,
            price: product.price,
            quantity: quantity,
            category: product.category,
          })
      } else {
        item$.set({
          key: product.key,
          title: product.title,
          price: product.price,
          category: product.category,
          imageUrl: product.imageUrl,
          quantity: 1
        })
      }

      // if (item) {
      //   item$.update({ product: product, quantity: (item.quantity || 0) + change });
      // } else {
      //   item$.set({ product: product, quantity: 1 });
      // }
    })
  }
}
