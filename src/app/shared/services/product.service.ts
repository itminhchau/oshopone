import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs'
import { Product } from '../models/product';
import { map } from 'rxjs/operators'

@Injectable()
export class ProductService {


  constructor(private db: AngularFireDatabase) { }
  async create(product) {
    const data = await this.db.list('/products').push(product);
    this.db.object(`/products/${data.key}`).update({
      key: data.key
    })
  }
  getAll(): Observable<Product[]> {
    return this.db.list<Product>('/products')
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => {
            const data = c.payload.val() as Product;
            const id = c.payload.key;
            return { id, ...data };
          })
        )
      );
  }
  get(productId) {
    return this.db.object('/products/' + productId);
  }
  update(productId, product) {
    return this.db.object('/products/' + productId).update(product);
  }
  delete(productId) {
    return this.db.object('/products/' + productId).remove();
  }
}
